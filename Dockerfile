FROM node as builder

WORKDIR /app

COPY package.json .
RUN npm install -g gatsby-cli
RUN npm install
RUN npm run build
COPY . .
CMD ["gatsby", "develop"]
